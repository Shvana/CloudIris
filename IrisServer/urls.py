"""IrisServer URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
import Iris.views as iris_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', iris_views.home, name='home'),
    path('setosa/', iris_views.setosa, name='setosa'),
    path('versicolor/', iris_views.versicolor, name='versicolor'),
    path('virginica/', iris_views.virginica, name='virginica'),
    path('apiform/', iris_views.apiform, name='apiform'),
    path('api/predict/', iris_views.api, name='predict'),
    path('analytics/', iris_views.analytics, name='analytics'),
]
